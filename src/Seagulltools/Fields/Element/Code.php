<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-06
 * Time: 15:29
 */

namespace Seagulltools\Fields\Element;

use Seagulltools\Fields\Field;

class Code extends Field
{
    public $component = 'code-component';

    public $showOnIndex = false;

    /**
     * Indicate that the code field is used to manipulate JSON.
     *
     * @return $this
     */
    public function json()
    {
        return $this->language(['application/ld+json']);
    }

    /**
     * Define the language syntax highlighting mode for the field.
     *
     * @param  string  $language
     * @return $this
     */
    public function language($language)
    {
        return $this->withMeta([
            'mode' => $language
        ]);
    }
}