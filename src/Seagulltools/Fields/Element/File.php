<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-21
 * Time: 11:25
 */

namespace Seagulltools\Fields\Element;

use Seagulltools\Fields\Field;

class File extends Field
{
    public $component = 'input-file-component';
}