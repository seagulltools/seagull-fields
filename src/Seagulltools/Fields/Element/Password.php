<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-06
 * Time: 15:06
 */

namespace Seagulltools\Fields\Element;

use Seagulltools\Fields\Field;

class Password extends Field
{
    public $component = 'input-password-component';

    public $showOnIndex = false;
    public $showOnDetail = false;
    public $showOnCreation = true;
    public $showOnUpdate = true;

    public function withConfirmation($confirmationField)
    {
        return $this->withMeta([
            'confirmation' => $confirmationField
        ]);
    }
}