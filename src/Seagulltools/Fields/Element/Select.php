<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-06
 * Time: 14:41
 */

namespace Seagulltools\Fields\Element;

use Seagulltools\Fields\Field;

class Select extends Field
{
    public $component = 'select-component';

    public function options($options)
    {
        return $this->withMeta([
            'options' => collect($options ?? [])->map(function ($label, $value) {
                return ['label' => $label, 'value' => $value];
            })->values()->all()
        ]);
    }
}