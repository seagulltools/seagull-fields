<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-06
 * Time: 14:12
 */

namespace Seagulltools\Fields\Element;

use Seagulltools\Fields\Field;

class Text extends Field
{
    public $component = 'input-text-component';

    public function copyable()
    {
        return $this->withMeta([
            'copyable' => true
        ]);
    }
}