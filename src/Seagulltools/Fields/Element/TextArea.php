<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-06
 * Time: 15:23
 */

namespace Seagulltools\Fields\Element;

use Seagulltools\Fields\Field;

class TextArea extends Field
{
    public $component = 'textarea-component';

    public $showOnIndex = false;
}