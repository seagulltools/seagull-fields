<?php
/**
 * Created by PhpStorm.
 * User: MatteoMeloni
 * Date: 2019-03-06
 * Time: 14:41
 */

namespace Seagulltools\Fields\Element;

use Seagulltools\Fields\Field;

class Toggle extends Field
{
    public $component = 'toggle-component';

    /**
     * @param mixed $conditions
     * @return Toggle
     */
    public function conditions($conditions)
    {
        [ $on, $off ] = array_keys($conditions);

        return $this->withMeta([
            'conditions' => [ 'on' => $on, 'off' => $off],
            'options' => collect($conditions ?? [])->map(function ($label, $value) {
                return ['label' => $label, 'value' => $value];
            })->values()->all(),
            'functions' => ''
        ]);
    }
}