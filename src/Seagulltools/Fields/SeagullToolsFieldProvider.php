<?php

namespace Seagulltools\Fields;

use Illuminate\Support\ServiceProvider;

class SeagullToolsFieldProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $initDirectory = __DIR__ . '/Element/';

        //Components
        $this->publishes([
           $initDirectory . '/components' => base_path('/resources/js/components/fields')
        ]);

    }
}
